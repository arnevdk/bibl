"""bibl is a minimalistic linter (style checker) for BibTeX.

bibl with support for libraries managed by JabRef.
bibl does not come with its own BibTeX parser, but leverages the pybtex parser.
"""
__version__ = "1.0.4"
